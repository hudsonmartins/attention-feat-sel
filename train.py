import os
import glob
import torch
from torch import optim, nn
import numpy as np
from utils import *
from superpoint_dataset import *
from skimage import io
from model2 import PoseEstimation
from tensorboardX import SummaryWriter
import matplotlib.pyplot as plt

def weighted_mse(output, target):
    K = 1#100.0
    return torch.mean((output[:, :3] - target[:, :3])**2 + K*(output[:, 3:] - target[:, 3:])**2)

def train_net(model, data_path, output_path, training_sequences, validation_sequences,
    logs_dir, checkpoints_dir, batch_size, lr, epochs, steps_per_epoch,
    early_stopping=True, save_best_only=True, model_name="best_model.pth"):

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    writer = SummaryWriter(logs_dir,
        comment= "_LR_"+ str(lr) + "_Batch_size_" + str(batch_size)) #tensorboard writer will output to logs directory
    print(f'Device found: {device}')
    print(f'Batch size: {batch_size}\nLearning rate: {lr}')
    model.to(device)
    optimizer = optim.Adam(model.parameters(), lr=lr)
    criterion = nn.MSELoss()        
    train_iterator = get_iterator(data_path, max_skip=1, cycle_every=None, max_kpts=500,
        batch_size=batch_size, sequences_names=training_sequences)
    epoch = 0
    best_loss = None
    best_epoch = -1
    epoch_loss = 0
    i = 0
    preds = []
    ys = []
    print('Starting epoch 0')
    for x, y in train_iterator.iterate():
        model.train()
        descs = torch.from_numpy(x[0]).to(device)
        kpts = torch.from_numpy(x[1]).to(device)
        optimizer.zero_grad()
        pred = model(descs.float(), kpts.float())
        y = torch.Tensor(y).to(device)
        loss = criterion(pred, y)
        preds.append(pred.cpu().detach().numpy()[0])
        ys.append(y.cpu().detach().numpy()[0])
        #loss = weighted_mse(pred, y)
        epoch_loss += loss.item()
        loss.backward()
        optimizer.step()
        i += 1
        print(f'{i}/{steps_per_epoch}')
        
        if(i%steps_per_epoch == 0):
            print(f'Epoch {epoch} finished. Loss: {epoch_loss/i}')
            title = 'Epoch_loss/train Parameters: '+' LR = ' + str(lr) + ' Batch_size = ' + str(batch_size)
            writer.add_scalar(title, (epoch_loss/i), epoch)
            """
            print("-"*20)
            print('STARTING VALIDATION')
            val_loss = 0
            val_mse = 0
            validation_size = steps_per_epoch
            means = train_iterator.means
            stds = train_iterator.stds
            valid_iterator = get_iterator(data_path, max_skip=1, cycle_every=validation_size, max_kpts=500,
                batch_size=batch_size, sequences_names=validation_sequences, randomize=False, means=means, stds=stds)
            #---------------------Validation--------------------------
            with torch.no_grad():
                i = j = 0
                for x, y in valid_iterator.iterate():
                    descs = torch.from_numpy(x[0]).to(device)
                    kpts = torch.from_numpy(x[1]).to(device)
                    model.eval()
                    pred = model(descs.float(), kpts.float())
                    y = torch.Tensor(y).to(device)
                    mse = criterion(pred, y)
                    wmse = weighted_mse(pred, y)
                    val_loss += wmse.item()
                    val_mse += mse.item()
                    j += 1
                    print(f'{j}/{validation_size}')

                    if(j == validation_size):
                        break
                print(f'validation loss: {val_loss/j}')
                print(f'validation mse: {val_mse/j}')
            title = 'Loss/validation Parameters: '+' LR = '+str(lr)+' Batch_size = '+str(batch_size)
            writer.add_scalar(title, (val_loss/j), epoch)
            writer.flush()
            
            print("="*20)
            if(best_loss is not None):
                if((val_loss/j) < best_loss):
                    best_loss = (val_loss/j)
                    best_epoch = epoch

                    if(save_best_only):
                        torch.save(model.state_dict(), os.path.join(checkpoints_dir, model_name))
                        print(f'Best model {epoch} saved!')

                if(not save_best_only):
                    torch.save(model.state_dict(), os.path.join(checkpoints_dir,  model_name+"_"+str(epoch)+".pth"))
                    print(f'Model {epoch} saved!')
            else:
                best_loss = (val_loss/j)
                best_epoch = epoch
                torch.save(model.state_dict(), os.path.join(checkpoints_dir, model_name+"_"+str(epoch)+".pth"))
                print(f'Model 0 saved!')
            """
            output_name = model_name+"_"+str(epoch)
            if(best_loss is not None):
                if((epoch_loss/i) < best_loss):
                    best_loss = epoch_loss/i
                    best_epoch = epoch
                torch.save(model.state_dict(), os.path.join(checkpoints_dir,  output_name+".pth"))
                print(f'Model {epoch} saved!')
            else:
                best_loss = epoch_loss/i
                best_epoch = epoch
                torch.save(model.state_dict(), os.path.join(checkpoints_dir, output_name+".pth"))
                print(f'Model 0 saved!')

            #print(f'Best validation loss {best_loss} in epoch {best_epoch}')
            print(f'Best training loss {best_loss} in epoch {best_epoch}')
            epoch += 1
            epoch_loss = 0
            preds = np.ravel(np.array(preds))
            ys = np.ravel(np.array(ys))
            plt.plot(range(len(preds)), preds, label="predictions")
            plt.plot(range(len(ys)), ys, "r--", label="true")
            plt.legend()
            plt.savefig(os.path.join(output_path, output_name+'.png'))
            plt.clf()
            preds, ys = [], []
            i = 0

            print(f'Starting epoch {epoch}')

            


def main(data_path, output_path, batch_size, lr, epochs, steps_per_epoch, load_model, model_name):
    
    logs_dir = os.path.join(output_path, 'logs')
    checkpoints_dir = os.path.join(output_path, 'models')
    os.makedirs(logs_dir, exist_ok=True)
    os.makedirs(checkpoints_dir, exist_ok=True)
    training_sequences = ['00', '02', '04', '05', '06', '08', '09']
    validation_sequences = ['10']

    model = PoseEstimation()
    if(load_model):
        model.load_state_dict(torch.load(str(load_model)))

    train_net(model, data_path, output_path, training_sequences, validation_sequences,
        logs_dir, checkpoints_dir, batch_size, lr, epochs, steps_per_epoch,
        early_stopping=True, save_best_only=False, model_name=model_name)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("inputs", default=None, help="Path to images and labels")
    parser.add_argument("outputs", default=None, help="Path to output data")
    parser.add_argument("--steps_per_epoch", default=500, type=int)
    parser.add_argument("--epochs", default=100, type=int)
    parser.add_argument("--batch_size", default=32, type=int)
    parser.add_argument("--lr", default=0.001, type=float)
    parser.add_argument("--load_model", default=None)
    parser.add_argument("--save_model_name", default="best_model.pth")
    args = parser.parse_args()

    main(args.inputs, args.outputs, args.batch_size, args.lr, args.epochs,
        args.steps_per_epoch, args.load_model, args.save_model_name)
