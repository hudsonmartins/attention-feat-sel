import torch
import torch.nn as nn
import torch.nn.functional as F

class AttentionModule(nn.Module):
    def __init__(self, kernel_size=1):
        super(AttentionModule, self).__init__()

        #Attention step
        self.conv1 = nn.Sequential(
            nn.Conv2d(in_channels=2, out_channels=1024, kernel_size=1,
                      padding=0),
            nn.BatchNorm2d(1024),
            nn.ReLU(inplace=True)
        )

        self.conv2 = nn.Sequential(
            nn.Conv2d(in_channels=1024, out_channels=2, kernel_size=1,
                      padding=0),
            nn.BatchNorm2d(2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d((2, 2)),
        )

        self.fc1 = nn.Linear(35000, 1000)
        self.softmax = nn.Softmax(dim=-1)

    def forward(self, x):
        batch_size, n_channels, rows, cols = x.size()
        x = self.conv1(x)
        x = self.conv2(x)
        x = x.view(-1, 35000) #flatten
        x = self.fc1(x)
        weights = self.softmax(x)
        attention = (weights > 0.5).float()*1 #threshold values
        attention = attention.reshape(batch_size, n_channels, rows, 1)
        return attention



class PosePredictionModule(nn.Module):
    def __init__(self):
        super(PosePredictionModule, self).__init__()
        self.fc1 = nn.Linear(1000*141, 512)
        self.bn = nn.BatchNorm1d(512)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(512, 6)

    def forward(self, x):
        x = self.fc1(x)
        x = self.bn(x)
        x = self.relu(x)
        x = self.fc2(x)
        return x


class AttentionPosePrediction(nn.Module):
    def __init__(self, attention, pose_pred):
        super(AttentionPosePrediction, self).__init__()
        self.attention = attention
        self.pose_pred = pose_pred

    def forward(self, input):
        scores = self.attention(input)
        x = scores*input
        x = x.view(-1, 141*1000) #flatten
        x = self.pose_pred(x)
        return x

class FeatureExtracion(nn.Module):
    def __init__(self):
        super(FeatureExtracion, self).__init__()
        self.conv_siamese = nn.Sequential(nn.Conv2d(3, 64, 7),
                                          nn.ReLU(inplace=True),
                                          nn.MaxPool2d(3),
                                          nn.Conv2d(64, 128, 1),
                                          nn.ReLU(),
                                          nn.Conv2d(128, 128, 3),
                                          nn.ReLU(),
                                          nn.MaxPool2d(2))

        self.conv1_1 = nn.Sequential(nn.Conv2d(256, 128, kernel_size=1),
                                    nn.BatchNorm2d(128),
                                    nn.ReLU(inplace=True))

        self.conv1_2 = nn.Sequential(nn.Conv2d(128, 128, kernel_size=1),
                                    nn.BatchNorm2d(128),
                                    nn.ReLU(inplace=True))

        self.conv2_1 = nn.Sequential(nn.Conv2d(128, 64, kernel_size=1),
                                    nn.BatchNorm2d(64),
                                    nn.ReLU(inplace=True))

        self.conv2_2 = nn.Sequential(nn.Conv2d(64, 32, kernel_size=1),
                                    nn.BatchNorm2d(32),
                                    nn.ReLU(inplace=True))

        self.conv3_1 = nn.Sequential(nn.Conv2d(32, 32, kernel_size=1),
                                    nn.BatchNorm2d(32),
                                    nn.ReLU(inplace=True))

        self.conv3_2 = nn.Sequential(nn.Conv2d(32, 16, kernel_size=1),
                                    nn.BatchNorm2d(16),
                                    nn.ReLU(inplace=True))

        self.dense1 = nn.Linear(16*60*204, 1024)
        self.bn = nn.BatchNorm1d(1024)
        self.relu = nn.ReLU()
        self.dense2 = nn.Linear(1024, 6)


    def forward(self, input):
        x1 = self.conv_siamese(input[:, 0, ...])
        x2 = self.conv_siamese(input[:, 1, ...])
        x = torch.cat((x1, x2), 1)
        x = self.conv1_1(x)
        x = self.conv1_2(x)
        x = self.conv2_1(x)
        x = self.conv2_2(x)
        x = self.conv3_1(x)
        x = self.conv3_2(x)
        x = x.view(-1, 16*60*204) #flatten
        x = self.dense1(x)
        x = self.bn(x)
        x = self.relu(x)
        x = self.dense2(x)
        return x


if __name__ == "__main__":
    import time
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # (N, C, H, W)
    fake_imgs = torch.randn(2, 2, 3, 376, 1241).to(device)
    print('input shape ', fake_imgs.shape)
    model = FeatureExtracion()
    model.to(device)
    init = time.time()
    # A full forward pass
    x = model(fake_imgs)
    print('output shape ', x.shape)
    print(x)
    print(time.time() - init)
