import csv
import glob
import numpy as np
import h5py
import os
import pandas as pd
import cv2

def read_csv(path):
    csv_arr = []
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=' ')
        line_count = 0
        for row in csv_reader:
            #print(row)
            timestamp = [row[0]]
            pose = [float(x) for x in row[1:]]
            csv_arr.append(timestamp+pose)
    return csv_arr

def loadh5(dump_file_full_name):
    ''' Loads a h5 file as dictionary '''
    if(os.path.exists(dump_file_full_name)):
        with h5py.File(dump_file_full_name, 'r') as h5file:
            dict_from_file = readh5(h5file)
        return dict_from_file
    print("File ",dump_file_full_name, " not found")
    return None

def readh5(h5node):
    ''' Recursive function to read h5 nodes as dictionary '''

    dict_from_file = {}
    for _key in h5node.keys():
        if isinstance(h5node[_key], h5py._hl.group.Group):
            dict_from_file[_key] = readh5(h5node[_key])
        else:
            dict_from_file[_key] = h5node[_key][()]
    return dict_from_file

def min_max_norm(v):
    xmin = min(v)
    xmax = max(v)
    return [(x - xmin)/(xmax-xmin) for x in v]


def convert2opencv_kpts(kp_list):
    return [cv2.KeyPoint(kp[0], kp[1], kp[2]) for kp in kp_list]


def get_only_pts(kp_list):
    return [(kp[0], kp[1]) for kp in kp_list]


def get_camera_params(dataset='kitti'):
    if(dataset=='kitti'):
        return 721.5377, (609.5593, 172.8540)
    elif(dataset=='euroc'):
         return 458.654, (367.215, 248.375)
