import os
import numpy as np
from utils import *
from convert import kitti_to_6dof
from sklearn import preprocessing
import sys

def limit_shape(arr, max_kpts):
    if(arr.shape[0] < max_kpts):
        #fill with zeros
        return np.pad(arr, [(0, max_kpts-arr.shape[0]), (0,0)], 'constant')
    elif(arr.shape[0] >= max_kpts):
        #cut array in the max number of rows
        return arr[:max_kpts, :]

def normalize_keypoints(kpts, image_shape):
    """ Normalize keypoints locations based on image image_shape"""
    height, width = image_shape
    center = (width//2, height//2)
    scaling = 0.7*np.max([height, width])
    return (kpts - center)/scaling

def to_network_input(descs, max_kpts, image_shape):
    desc = limit_shape(descs['descriptors'], max_kpts)#.flatten()
    kpt = limit_shape(descs['keypoints'], max_kpts)#.flatten()
    _kpt = kpt[:, :2] #(x,y)
    _kpt = normalize_keypoints(_kpt, image_shape)
    
    _kpt = np.append(_kpt, np.expand_dims(kpt[:, 4], axis=1), axis=-1)  #concatenating with response (score)
    return desc, _kpt

def get_means_and_stds(ys):
    deltas = []
    for seq in ys:
        for i in range(len(seq)-1):
            delta = kitti_to_6dof(seq[i+1])-kitti_to_6dof(seq[i])
            deltas.append(delta)
    deltas = np.array(deltas)
    means = np.mean(deltas, axis=0)
    stds = np.std(deltas, axis=0)
    return means, stds


def get_iterator(data_path, cycle_every, batch_size, max_kpts, sequences_names, 
    max_skip, randomize=True, means=None, stds=None):
    random_seed = 42
    rand = np.random.RandomState(random_seed)
    return Dataset(data_path, batch_size=batch_size,  max_kpts=max_kpts, image_size=(376, 1241),
        max_skip=max_skip, rand=rand, randomize=randomize, cycle_every=cycle_every, 
        sequences_names=sequences_names, means=means, stds=stds)


class Dataset():
    def __init__(self, data_path, batch_size, max_kpts, sequences_names, image_size, max_skip,
        rand, randomize=True, cycle_every=None, means=None, stds=None):

        self._ys = []
        for sequence in sequences_names:
            with open(os.path.join(data_path+'/poses/',  sequence+'.txt')) as file:
                poses = np.array([[float(x) for x in line.split()] for line in file],
                    dtype=np.float32)
            self._ys.append(poses)

        if(means is not None and stds is not None):
            self.means = means
            self.stds = stds
        else:
            self.means, self.stds = get_means_and_stds(self._ys)

        print('means: ', self.means, '\stds: ', self.stds)
        self._batch_size = batch_size
        self._rand = rand
        self._rand_state = self._rand.get_state()
        self._data_path = data_path
        self.sequences_names = sequences_names
        self._cycle_every = cycle_every
        self._n_iterations = 0
        self._sequences = list(range(len(self._ys)))
        self._curr_sequence = 0
        self._last_frame = 0
        self._img_size = image_size
        self._max_skip = max_skip+1
        self._max_kpts = max_kpts
        self._randomize = randomize

    def reset(self):
        self._rand.set_state(self._rand_state)
        self._n_iterations = 0

    def get_batch(self):
        if self._cycle_every is not None and self._n_iterations > 0 \
                and self._n_iterations % self._cycle_every == 0:
            self.reset()

        descs, kpts, ys = [], [], []
        for _ in range(self._batch_size):
            #First image
          
            if(self._randomize):
                self._curr_sequence = self._rand.choice(list(range(len(self._ys))))
            else:
                if(self._last_frame >= len(self._ys[self._curr_sequence])-self._max_skip):
                    self._curr_sequence += 1
                    self._last_frame = 0
                    if(self._curr_sequence == len(self._sequences)):
                        self._curr_sequence = 0

            sequence = self._curr_sequence

            #-------LOADING KEYPOINTS-------
            x1 = None
            if(self._randomize):
                index = self._rand.choice(list(range(len(self._ys[sequence]))))
            else:
                index = self._last_frame
            kpt_path = os.path.join(self._data_path+'/sequences/', self.sequences_names[sequence],
                'original/descs', '%06d'%index+'.png_desc.h5')
            x1 = loadh5(kpt_path)

            #Second image
            #Choose a frame between t+1 and t+max_skip
            x2 = None
            if(self._randomize):
                pair_index = index + self._rand.choice(list(range(1, self._max_skip)))
            else:
                pair_index = index + 1
            if(pair_index >= len(self._ys[sequence])):
                pair_index = index
            kpt_path = os.path.join(self._data_path+'/sequences/', self.sequences_names[sequence],
                'original/descs', '%06d'%pair_index+'.png_desc.h5')
            x2 = loadh5(kpt_path)

            self._last_frame = pair_index

            #Convert to input
            desc1, kpt1 = to_network_input(x1, self._max_kpts, self._img_size)
            desc2, kpt2 = to_network_input(x2, self._max_kpts, self._img_size)
            desc = np.array([desc1, desc2])
            kpt = np.array([kpt1, kpt2])
            x = [desc, kpt]

            #-------LOADING POSES-------
            pose1 = kitti_to_6dof(self._ys[sequence][index])
            pose2 = kitti_to_6dof(self._ys[sequence][pair_index])
            #y = (pose2-self.means)/self.stds - (pose1-self.means)/self.stds
            y = pose2-pose1

            descs.append(desc)
            kpts.append(kpt)
            ys.append(y)

        descs, kpts, ys = np.stack(descs, axis=0), np.stack(kpts, axis=0), np.stack(ys, axis=0)
        self._n_iterations += 1

        return ([descs, kpts], ys)
       
    def iterate(self):
        while True:
            yield self.get_batch()

if __name__ == '__main__':
    rand = np.random.RandomState(42)
    it = get_iterator('/home/hudson/Desktop/Unicamp/Mestrado/Projeto/datasets/kitti/', cycle_every=None, batch_size=1,
        max_kpts=500, sequences_names=['00', '02'], max_skip=5)
    for x, y in it.iterate():
        print(x[1])
        print(x[0].shape, x[1].shape, y.shape)
        #break