from copy import deepcopy
from pathlib import Path
import torch
from torch import nn
import torch.nn.functional as F
from skimage import io
from models.superglue import SuperGlue

    
class OutputPose(nn.Module):
    default_config = {
        'num_keypoints': 500,
    }
    def __init__(self, config={}):
        super(OutputPose, self).__init__()
        self.config = {**self.default_config, **config}
        
        self.conv1 = nn.Conv1d(in_channels=500, out_channels=512, kernel_size=3, padding=1)
        self.conv2 = nn.Conv1d(in_channels=512, out_channels=256, kernel_size=3, padding=1)
        self.conv3 = nn.Conv1d(in_channels=256, out_channels=128, kernel_size=3, padding=1) 
        self.conv4 = nn.Conv1d(in_channels=128, out_channels=64, kernel_size=3, padding=1) 
        self.conv5 = nn.Conv1d(in_channels=64, out_channels=32, kernel_size=3, padding=1) 
        self.conv6 = nn.Conv1d(in_channels=32, out_channels=2, kernel_size=3, padding=1) 
        """
        self.conv1_1 = nn.Conv2d(in_channels=1, out_channels=512, kernel_size=(1, 500))
        self.conv1_2 = nn.Conv2d(in_channels=512, out_channels=256, kernel_size=3, padding=1)
        self.conv1_3 = nn.Conv2d(in_channels=256, out_channels=128, kernel_size=3, padding=1)
        self.conv1_4 = nn.Conv2d(in_channels=128, out_channels=64, kernel_size=3, padding=1)
        self.conv1_5 = nn.Conv2d(in_channels=64, out_channels=1, kernel_size=3, padding=1)

        self.conv2_1 = nn.Conv2d(in_channels=1, out_channels=512, kernel_size=(500, 1))
        self.conv2_2 = nn.Conv2d(in_channels=512, out_channels=256, kernel_size = 3, padding=1)
        self.conv2_3 = nn.Conv2d(in_channels=256, out_channels=128, kernel_size = 3, padding=1)
        self.conv2_4 = nn.Conv2d(in_channels=128, out_channels=64, kernel_size = 3, padding=1)
        self.conv2_5 = nn.Conv2d(in_channels=64, out_channels=1, kernel_size = 3, padding=1)
        """
        self.softmax = nn.Softmax(dim=1)
        
        self.fc1 = nn.Linear(500*6, 2048)
        self.bn1 = nn.BatchNorm1d(2048)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(2048, 1024)
        self.bn2 = nn.BatchNorm1d(1024)
        self.fc3 = nn.Linear(1024, 512)
        self.bn3 = nn.BatchNorm1d(512)
        self.fc4 = nn.Linear(512, 256)
        self.bn4 = nn.BatchNorm1d(256)        
        self.out = nn.Linear(256, 6)


    def forward(self, x0, x1, matching_scores):
        """
        w0 = self.conv1_1(matching_scores.unsqueeze(1))
        w0 = self.relu(w0)
        w0 = self.conv1_2(w0)
        w0 = self.relu(w0)
        w0 = self.conv1_3(w0)
        w0 = self.relu(w0)
        w0 = self.conv1_4(w0)
        w0 = self.relu(w0)
        w0 = self.conv1_5(w0)
        w0 = self.softmax(w0)
        w0 = w0[:, 0]

        w1 = self.conv2_1(matching_scores.unsqueeze(1))
        w1 = self.relu(w1)
        w1 = self.conv2_2(w1)
        w1 = self.relu(w1)
        w1 = self.conv2_3(w1)
        w1 = self.relu(w1)
        w1 = self.conv2_4(w1)
        w1 = self.relu(w1)
        w1 = self.conv2_5(w1)
        w1 = self.relu(w1)
        w1 = self.softmax(w1)
        w1 = torch.transpose(w1, 2, -1)
        w1 = w1[:, 0]

        """
        x = self.conv1(matching_scores)
        x = self.relu(x)
        x = self.conv2(x)
        x = self.relu(x)
        x = self.conv3(x)
        x = self.relu(x)
        x = self.conv4(x)
        x = self.relu(x)
        x = self.conv5(x)
        x = self.relu(x)
        x = self.conv6(x)
        x = self.softmax(x)
        w0 = x[:,0].unsqueeze(-1)
        w1 = x[:,1].unsqueeze(-1)
        
        x0 = x0 * w0
        x1 = x1 * w1

        x = torch.cat((x0, x1), 2)
        x = x.view(-1, 500*6)
        x = self.fc1(x)
        x = self.bn1(x)
        x = self.relu(x)

        x = self.fc2(x)
        x = self.bn2(x)
        x = self.relu(x)

        x = self.fc3(x)
        x = self.bn3(x)
        x = self.relu(x)

        x = self.fc4(x)
        x = self.bn4(x)
        x = self.relu(x)
                
        x = self.out(x)
        
        return x

class PoseEstimation(nn.Module):
    
    def __init__(self):
        super().__init__()
        config = {
            'descriptor_dim': 256,
            'weights': 'outdoor',
            'keypoint_encoder': [32, 64, 128, 256],
            'GNN_layers': ['self', 'cross'] * 9,
            'sinkhorn_iterations': 100,
            'match_threshold': 0.2,
        }
        self.matcher = SuperGlue(config)
        self.output = OutputPose()
        
    def forward(self, descs, kpts):
        desc0, desc1 = descs[:, 0], descs[:, 1] #(batch, img)
        kpts0, kpts1 = kpts[:, 0], kpts[:, 1] #(batch, img)
        data = {'descriptors0': desc0,
                'descriptors1': desc1,
                'keypoints0': kpts0[:, :, :2],
                'keypoints1': kpts1[:, :, :2],
                'scores0': kpts0[:, :, 2],
                'scores1': kpts1[:, :, 2]}

        matching_scores = self.matcher(data)
        out = self.output(kpts0, kpts1, matching_scores)
        
        return out

   
if __name__ == '__main__':
    import time
    import numpy as np
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    
    # (N, C, H, W)
    descs = torch.randn(3, 2, 500, 256).to(device)
    kpts = torch.randn(3, 2, 500, 3).to(device)
    
    model = PoseEstimation()
    model.to(device)
    init = time.time()
    # A full forward pass
    x = model(descs, kpts)
    print("out ", x.shape)
    print(time.time() - init)