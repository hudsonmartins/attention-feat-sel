import torch
import cv2
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from models.superglue import SuperGlue
from models.superpoint import SuperPoint
from models.utils import make_matching_plot_fast


def limit_shape(arr, max_kpts):
    if(arr.shape[0] < max_kpts):
        #fill with zeros
        return np.pad(arr, [(0, max_kpts-arr.shape[0]), (0,0)])

    elif(arr.shape[0] >= max_kpts):
        #cut array in the max number of rows
        return arr[:max_kpts, ...]


def normalize_keypoints(kpts, image_shape):
    """ Normalize keypoints locations based on image image_shape"""
    height, width = image_shape
    center = (width//2, height//2)
    scaling = 0.7*np.max([height, width])
    return (kpts - center)/scaling


def frame2tensor(frame, device):
    return torch.from_numpy(frame/255.).float()[None, None].to(device)


def to_network_input(pred, max_kpts, image_shape):
    desc = pred['descriptors'][0].cpu().detach().numpy()
    desc = desc.transpose()    
    desc = limit_shape(desc, max_kpts)
    kpt = limit_shape(pred['keypoints'][0].cpu().detach().numpy(), max_kpts)
    scores = pred['scores'][0].cpu().detach().numpy()
    scores = np.expand_dims(scores, axis=1)
    scores = limit_shape(scores, max_kpts)
    #kpt = normalize_keypoints(kpt, image_shape)
    kpt = np.append(kpt, scores, axis=-1)  #concatenating with scores
    return desc, kpt


def log_sinkhorn_iterations(Z, log_mu, log_nu, iters: int):
    """ Perform Sinkhorn Normalization in Log-space for stability"""
    u, v = torch.zeros_like(log_mu), torch.zeros_like(log_nu)
    for _ in range(iters):
        u = log_mu - torch.logsumexp(Z + v.unsqueeze(1), dim=2)
        v = log_nu - torch.logsumexp(Z + u.unsqueeze(2), dim=1)
    return Z + u.unsqueeze(2) + v.unsqueeze(1)


def log_optimal_transport(scores, alpha, iters: int):
    """ Perform Differentiable Optimal Transport in Log-space for stability"""
    b, m, n = scores.shape
    one = scores.new_tensor(1)
    ms, ns = (m*one).to(scores), (n*one).to(scores)

    bins0 = alpha.expand(b, m, 1)
    bins1 = alpha.expand(b, 1, n)
    alpha = alpha.expand(b, 1, 1)

    couplings = torch.cat([torch.cat([scores, bins0], -1),
                           torch.cat([bins1, alpha], -1)], 1)

    norm = - (ms + ns).log()
    log_mu = torch.cat([norm.expand(m), ns.log()[None] + norm])
    log_nu = torch.cat([norm.expand(n), ms.log()[None] + norm])
    log_mu, log_nu = log_mu[None].expand(b, -1), log_nu[None].expand(b, -1)

    Z = log_sinkhorn_iterations(couplings, log_mu, log_nu, iters)
    Z = Z - norm  # multiply probabilities by M+N
    return Z


def arange_like(x, dim: int):
    return x.new_ones(x.shape[dim]).cumsum(0) - 1  # traceable in 1.1

class extractor():
    def __init__(self):
        self._device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.superpoint = SuperPoint({}).to(self._device)

    def extract(self, img1, img2):
        pred1 = self.superpoint({'image': frame2tensor(img1, self._device)})
        pred2 = self.superpoint({'image':  frame2tensor(img2, self._device)})
        
        #Convert to input
        desc1, kpt1 = to_network_input(pred1, 600, img1.shape)
        desc2, kpt2 = to_network_input(pred2, 600, img1.shape)
        descs = np.array([desc1, desc2])
        kpts = np.array([kpt1, kpt2])
        return descs, kpts


class matcher():
    def __init__(self):
        config = {
            'descriptor_dim': 256,
            'weights': 'outdoor',
            'keypoint_encoder': [32, 64, 128, 256],
            'GNN_layers': ['self', 'cross'] * 9,
            'sinkhorn_iterations': 100,
            'match_threshold': 0.2,
        }
        self.config = {**config}
        self.matcher = SuperGlue(config)
        
    def match(self, descs, kpts, img0_shape, img1_shape):
        desc0, desc1 = torch.from_numpy(descs[0]).float(), torch.from_numpy(descs[1]).float()
        kpts0, kpts1 = torch.from_numpy(kpts[0]).float(), torch.from_numpy(kpts[1]).float()
        data = {'descriptors0': desc0.unsqueeze(0),
                'descriptors1': desc1.unsqueeze(0),
                'keypoints0': kpts0[:, :2].unsqueeze(0),
                'keypoints1': kpts1[:, :2].unsqueeze(0),
                'scores0': kpts0[:, 2].unsqueeze(0),
                'scores1': kpts1[:, 2].unsqueeze(0),
                'img0_shape': img0_shape,
                'img1_shape': img1_shape
                }

        matching_scores = self.matcher(data)
        bin_score = torch.nn.Parameter(torch.tensor(1.))

        scores = log_optimal_transport(
            matching_scores, bin_score,
            iters=self.config['sinkhorn_iterations'])

        # Get the matches with score above "match_threshold".
        max0, max1 = scores[:, :-1, :-1].max(2), scores[:, :-1, :-1].max(1)
        indices0, indices1 = max0.indices, max1.indices
        mutual0 = arange_like(indices0, 1)[None] == indices1.gather(1, indices0)
        mutual1 = arange_like(indices1, 1)[None] == indices0.gather(1, indices1)
        zero = scores.new_tensor(0)
        mscores0 = torch.where(mutual0, max0.values.exp(), zero)
        mscores1 = torch.where(mutual1, mscores0.gather(1, indices1), zero)

        valid0 = mutual0 & (mscores0 > self.config['match_threshold'])
        valid1 = mutual1 & valid0.gather(1, indices1)
        indices0 = torch.where(valid0, indices0, indices0.new_tensor(-1))
        indices1 = torch.where(valid1, indices1, indices1.new_tensor(-1))

        return {
            'matches0': indices0, # use -1 for invalid match
            'matches1': indices1, # use -1 for invalid match
            'matching_scores0': mscores0,
            'matching_scores1': mscores1,
        }


if __name__ == '__main__':
    import time
    import numpy as np
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    path = '/home/hudson/Desktop/Unicamp/Doutorado/Projeto/datasets/kitti/sequences/03/image_2/'

    img1 = cv2.imread(os.path.join(path, "000000.png"), cv2.IMREAD_GRAYSCALE)
    img2 = cv2.imread(os.path.join(path, "000001.png"), cv2.IMREAD_GRAYSCALE)
    img1 = cv2.resize(img1, (640, 480))
    img2 = cv2.resize(img2, (640, 480))

    e = extractor()
    descs, kpts = e.extract(img1, img2)
    
    m = matcher ()
    matches = m.match(descs, kpts, img1.shape, img2.shape)
    kpts0 = kpts[0, :, :2]
    kpts1 = kpts[1, :, :2]
    confidence = matches['matching_scores0'][0].cpu().detach().numpy()
    matches = matches['matches0'][0].cpu().detach().numpy()
    valid = matches > -1
    mkpts0 = kpts0[valid]
    mkpts1 = kpts1[matches[valid]]
    color = cm.jet(confidence[valid])

    #height, width = img1.shape
    #center = (width//2, height//2)
    #scaling = 0.7*np.max([height, width])
    #kpts0 = kpts0*scaling+center
    #mkpts0 = mkpts0*scaling+center
    #kpts1 = kpts1*scaling+center
    #mkpts1 = mkpts1*scaling+center
    text = ['SuperGlue',
            'Keypoints: {}:{}'.format(len(kpts0), len(kpts1)),
            'Matches: {}'.format(len(mkpts0)),]
    k_thresh = 0.005
    m_thresh = 0.2
    small_text = ['Keypoint Threshold: {:.4f}'.format(k_thresh),
                'Match Threshold: {:.2f}'.format(m_thresh),
                ]
        
    make_matching_plot_fast(img1, img2, kpts0, kpts1, mkpts0, mkpts1, color, text=text, small_text=small_text,
            path='/home/hudson/Desktop/Unicamp/Doutorado/Projeto/Codigos/attention-feat-sel/matches.png', show_keypoints=True)